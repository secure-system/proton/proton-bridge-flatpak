## How to install

<a href='https://gitlab.com/secure-system/proton/proton-bridge-flatpak/raw/master/ch.protonmail.protonmail-bridge.flatpakref?inline=false'><img width='240' alt='Download Flatpak' src='assets/download-badge.svg'/></a>

Or at the command line,

````bash
flatpak install --user https://gitlab.com/secure-system/proton/proton-bridge-flatpak/raw/master/ch.protonmail.protonmail-bridge.flatpakref
````
